"""Generate the code reference pages and navigation."""

from pathlib import Path

import mkdocs_gen_files  # type: ignore # noqa

nav = mkdocs_gen_files.Nav()

root = Path(__file__).parent.parent
src = root

for path in sorted(src.glob("*.md")):
    if not path.is_file():
        continue
    module_path = path.relative_to(src)
    doc_path = path.relative_to(src).with_suffix(".md")
    full_doc_path = Path("root", doc_path)

    parts = tuple(module_path.parts)
    nav[parts] = doc_path.as_posix()

    with mkdocs_gen_files.open(full_doc_path, "w") as fd:
        fd.write(module_path.read_text(encoding="utf-8"))

    mkdocs_gen_files.set_edit_path(full_doc_path, path.relative_to(root))

with mkdocs_gen_files.open("root/SUMMARY.md", "w") as nav_file:
    nav_file.writelines(nav.build_literate_nav())
