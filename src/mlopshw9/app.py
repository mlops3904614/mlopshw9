"""
FastAPI app.
"""

import os
import logging

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from mlopshw9.api.routes.router import router as api_router

from mlopshw9 import __version__

logger = logging.getLogger(__name__)


def get_app() -> FastAPI:
    """
    FastAPI app initialization.
    """
    fastapi_app = FastAPI(
        title="Emotion classification service",
        version=__version__,
        debug=(os.environ.get("DEBUG", "False") == "True"),
        description="ML service for classifying emotions in text",
    )
    fastapi_app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["GET", "POST"],
        allow_headers=["*"],
    )
    fastapi_app.include_router(api_router, prefix="/api")
    logger.info("FastAPI application has been initialized")
    return fastapi_app


app = get_app()
