"""
Predict endpoint.
"""

from celery.result import AsyncResult
from fastapi import APIRouter, BackgroundTasks

from mlopshw9 import log_request
from mlopshw9.schemas.requests import TextRequest
from mlopshw9.services.model import predict

router = APIRouter()


@router.post("/create_task/")
async def predict_emotion(text_request: TextRequest, background_tasks: BackgroundTasks):
    """
    Предсказание эмоции по тексту.

    Args:
        text_request (TextRequest): Текстовый запрос.

    Returns:
        dict: id задачи celery
    """
    task = predict.delay(text=text_request.text)
    background_tasks.add_task(
        log_request,
        "/create_task/",
        {"text_request": text_request},
        {"task_id": task.id},
    )
    return {"task_id": task.id}


@router.get("/task_result/{task_id}")
async def get_status(task_id: str, background_tasks: BackgroundTasks):
    """
    Предсказание эмоции по тексту.

    Args:
        task_id (str): id задачи celery

    Returns:
        dict: id задачи celery, её статус и результат
    """
    task_result = AsyncResult(task_id)
    background_tasks.add_task(
        log_request,
        f"/task_result/{task_id}",
        {"task_id": task_id},
        {
            "task_id": task_id,
            "task_status": task_result.status,
            "task_result": task_result.result,
        },
    )
    return {
        "task_id": task_id,
        "task_status": task_result.status,
        "task_result": task_result.result,
    }
