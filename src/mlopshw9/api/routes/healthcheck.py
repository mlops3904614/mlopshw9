"""
Healthcheck endpoint.
"""

import datetime

from fastapi import APIRouter, BackgroundTasks

from mlopshw9 import log_request
from mlopshw9.schemas.healthcheck import HealthcheckResult

router = APIRouter()


@router.get("/healthcheck/", response_model=HealthcheckResult, name="healthcheck")
def get_health_check(background_tasks: BackgroundTasks) -> HealthcheckResult:
    """
    Healthcheck endpoint.
    """
    health_check = HealthcheckResult(
        is_alive=True, date=datetime.datetime.utcnow().isoformat()
    )
    background_tasks.add_task(
        log_request,
        "/healthcheck",
        {},
        health_check,
    )
    return health_check
