"""
Creating router.
"""

from fastapi import APIRouter

from mlopshw9.api.routes import (
    predict,
    healthcheck,
)

router = APIRouter()

router.include_router(predict.router, tags=["predict"])
router.include_router(healthcheck.router, tags=["healthcheck"])
