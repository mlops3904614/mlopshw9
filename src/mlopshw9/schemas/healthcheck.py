"""
Schemas for healthcheck.
"""

from pydantic import BaseModel


class HealthcheckResult(BaseModel):
    """
    Schema for healthcheck.
    """

    is_alive: bool
    date: str
