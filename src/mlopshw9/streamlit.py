"""
Streamlit demo
"""

import time

import pandas as pd
import requests
import streamlit as st

app_config = {
    "mode": {"text": "Текст", "delimited_text": "Текст (разделить)"},
    "status": {"loading": "Загрузка...", "error": "Ошибка (", "done": "Готово )"},
    "other": {
        "title": "Сервис для эмоциональной оценки текста",
        "pos": "Положительное",
        "neg": "Отрицательное",
        "input_text": "Текст для оценки",
        "input_delimited_text": "Разделитель",
        "submit": "Оценить",
        "example1": "Я тебя люблю!",
        "example2": "Я тебя люблю!\nЯ тебя ненавижу\nЯ тебя знаю",
        "default_delimited": r"\n",
        "mode": "Режим",
        "class": "Тон",
        "score": "Уверенность",
    },
}


@st.cache_data
def get_answer(text):
    task_id = requests.post(
        "http://127.0.0.1:8000/api/create_task/", json={"text": text}
    ).json()["task_id"]
    while True:
        time.sleep(1)
        res = requests.get(
            f"http://127.0.0.1:8000/api/task_result/{task_id}/", json={"text": "Hi"}
        ).json()
        if res["task_status"] == "SUCCESS":
            return res["task_result"]


def get_answers(text, delimiter=None):
    if not delimiter:
        return [get_answer(text)]
    else:
        delimiter = delimiter.replace("\\n", "\n").replace("\\t", "\t")
        return [get_answer(i) for i in text.split(delimiter)]


def update():
    global text, delimiter
    with result_container:
        with st.status(app_config["status"]["loading"]) as status:
            result = get_answers(text, delimiter)
            status.update(
                label=app_config["status"]["done"], state="complete", expanded=False
            )
        df = pd.DataFrame(
            result, columns=[app_config["other"]["class"], app_config["other"]["score"]]
        )
        result_container.table(df)


st.title(app_config["other"]["title"])

mode = st.selectbox(
    label=app_config["other"]["mode"],
    options=list(app_config["mode"].values()),
)

if mode == app_config["mode"]["text"]:
    text = st.text_area(
        app_config["other"]["input_text"],
        key="simple-text",
        value=app_config["other"]["example1"],
    )
    delimiter = None
elif mode == app_config["mode"]["delimited_text"]:
    text = st.text_area(
        app_config["other"]["input_text"],
        key="text_with_d",
        value=app_config["other"]["example2"],
    )
    delimiter = st.text_input(
        app_config["other"]["input_delimited_text"],
        value=app_config["other"]["default_delimited"],
    )

st.button(app_config["other"]["submit"], on_click=update)

result_container = st.container()
