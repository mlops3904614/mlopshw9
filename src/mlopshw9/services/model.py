"""
Service for model
Using Celery
"""

import os

import celery as celery_module
from transformers import pipeline

celery = celery_module.Celery()
celery.conf.broker_url = os.environ.get("CELERY_BROKER_URL", "amqp://localhost:5672")
celery.conf.result_backend = os.environ.get(
    "CELERY_RESULT_BACKEND", "redis://localhost:6379"
)


class Model:
    """
    Model
    """

    MODEL_NAME = "seara/rubert-tiny2-russian-sentiment"

    def __init__(self) -> None:
        """Init model"""
        self.model = pipeline(model=self.MODEL_NAME)

    def __call__(
        self, text: str | list[str]
    ) -> tuple[str, float] | list[tuple[str, float]]:
        """Predict"""
        if isinstance(text, str):
            return self.model(text)[0]["label"], self.model(text)[0]["score"]
        if isinstance(text, list):
            return self.model(text)
        raise ValueError("text must be str or list[str]")


def get_model() -> Model:
    """Singleton for model"""
    if not hasattr(get_model, "instance"):
        get_model.instance = Model()
    return get_model.instance


@celery_module.shared_task
def predict(text: str | list[str]) -> tuple[str, float] | list[tuple[str, float]]:
    """Celery entrypoint for predict"""
    return get_model()(text=text)
