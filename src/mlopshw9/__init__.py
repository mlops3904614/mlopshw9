"""Приложение для анализа тональности"""

import logging
import logging.config
import os

from mlopshw9.__version__ import __version__  # noqa


def _setlogger():
    """Настройка логгера"""
    ROOT_LEVEL = os.environ.get("PROD", "INFO")
    LOGGING_CONFIG = {
        "version": 1,
        "disable_existing_loggers": True,
        "formatters": {
            "standard": {"format": "%(asctime)s [%(levelname)s] %(name)s: %(message)s"},
        },
        "handlers": {
            "default": {
                "level": "INFO",
                "formatter": "standard",
                "class": "logging.StreamHandler",
                "stream": "ext://sys.stdout",  # Default is stderr
            },
        },
        "loggers": {
            "": {  # root logger
                "level": ROOT_LEVEL,  # "INFO",
                "handlers": ["default"],
                "propagate": False,
            },
            "uvicorn.error": {
                "level": "DEBUG",
                "handlers": ["default"],
            },
            "uvicorn.access": {
                "level": "DEBUG",
                "handlers": ["default"],
            },
        },
    }
    logging.config.dictConfig(LOGGING_CONFIG)
    logger = logging.getLogger(__name__)
    return logger


def log_request(url: str, input: dict = dict(), output: dict = dict()):
    """Логирование запросоа и из результатов"""
    global _logger
    logger = logging.getLogger(__name__)
    logger.info(f"I [fastapi] process {url}, get {input} and return {output}")


_logger = _setlogger()
