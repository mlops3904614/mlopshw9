"""Command line интерфейс"""

import os
import argparse
import uvicorn


def main():
    """Настройка argparse"""
    parser = argparse.ArgumentParser(description="Manage the server.")

    subparsers = parser.add_subparsers(dest="command")

    runserver_parser = subparsers.add_parser(
        "runserver", help="Run the server (only debug)"
    )
    runserver_parser.add_argument(
        "--host", type=str, default="127.0.0.1", help="The host to bind to"
    )
    runserver_parser.add_argument(
        "--port", type=int, default=8000, help="The port to bind to"
    )
    runserver_parser.add_argument(
        "--reload", action="store_true", help="Enable auto-reload"
    )

    args = parser.parse_args()

    if args.command == "runserver":
        os.environ["DEBUG"] = "True"
        uvicorn.run(
            "mlopshw9.app:app", host=args.host, port=args.port, reload=args.reload
        )
    else:
        parser.print_help()


if __name__ == "__main__":
    main()
